# svkcl

[![NPM](https://github.com/FL03/svkcl/actions/workflows/npm.yml/badge.svg)](https://github.com/FL03/svkcl/actions/workflows/npm.yml)

***

A complete component library for creating elegant, data-centric applications in SvelteKit

## Getting Started

### Building from the source

#### _Clone the repository_

```bash
git clone https://github.com/FL03/svkcl
```

### Usage

#### _Install the dependencies_

```bash
npm install
```

#### _Build the workspace_

```bash
npm run build
```

#### _Run the demo application_

```bash
npm run dev
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

- [Apache-2.0](https://choosealicense.com/licenses/apache-2.0/)
- [MIT](https://choosealicense.com/licenses/mit/)
